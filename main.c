#include <avr/io.h>
#define F_CPU 1000000UL
#include <util/delay.h>
#include <stdlib.h>
#include <avr/interrupt.h>
#include <util/twi.h>


//           Nibble:    8 4 2 1 8  4  2  1
// MCP23008 addresses = 0 1 0 0 A2 A1 A0 R/W
//                     [_______] V  V  V  V
//                         V     |  |  |  |
//  given by manufacturer -+     |  |  |  |
//  set by PIN 3 ----------------+  |  |  |
//  set by PIN 4 -------------------+  |  |
//  set by PIN 5 ----------------------+  |
//  READ = 1, WRITE = 0 ------------------+
#define ADDR_MCP23008_1 0x40
#define ADDR_MCP23008_2 0x42
#define TWI_READ        0x01  // TWI-read := LSB=1

// MSC23008 registers
#define IODIR_MCP23008  0x00
#define GPIO_MCP23008   0x09

#define RETRY           100

#define DEBUGDELAY      2000


uint8_t errorcounter = 0;


uint8_t TWI_status(void) {
    uint8_t tw_status = TW_STATUS;
    uint8_t actions[][2] = {
        { TW_START,         0x01 },
        { TW_MT_SLA_ACK,    0x02 },
        { TW_MT_DATA_ACK,   0x04 },

        { TW_REP_START,     0x11 },
        { TW_MT_SLA_NACK,   0x82 },
        { TW_MT_DATA_NACK,  0x84 },

        { TW_MT_ARB_LOST,   0xFF }
    };
    uint8_t action = 0;

    for(uint8_t i=0; i<=sizeof(actions); i++) {
        if( tw_status == actions[i][0] ) {
            action = actions[i][1];
            PORTD = action;
            _delay_ms(1000);
            PORTD = 0x00;
            _delay_ms(1000);
        }
    }

    return action;

}


void TWI_enable(void) {
    // enable twi
    //TWCR |= _BV(TWEA) | _BV(TWEN) | _BV(TWIE);
    TWCR |= _BV(TWEA) | _BV(TWEN);
}


void TWI_start(void) {
    // start
    TWCR = _BV(TWINT) | _BV(TWSTA) | _BV(TWEN);
    while(!(TWCR & _BV(TWINT)));
}


void TWI_stop(void) {
    // stop
    TWCR = _BV(TWINT) | _BV(TWEN) | _BV(TWSTO);
    while(!(TWCR & _BV(TWSTO)));
}


void TWI_write(uint8_t byte) {
    // write address
    TWDR = byte;
    TWCR = _BV(TWINT) | _BV(TWEN);
    while(!(TWCR & _BV(TWINT)));
}


void MCP23008_write(uint8_t addr, uint8_t mcp_register, uint8_t data) {

    TWI_start();
    TWI_write(addr & ~TWI_READ);
    TWI_write(mcp_register);
    TWI_write(data);
    TWI_stop();

}


uint8_t MCP23008_write_error_aware(uint8_t addr, uint8_t mcp_register, uint8_t data) {
    uint8_t success = 0;

    for(uint8_t i=0; i <= RETRY; i++ ) {

        TWCR = _BV(TWINT) | _BV(TWSTA) | _BV(TWEN);
        while(!(TWCR & _BV(TWINT)));
        if( (TW_STATUS != TW_START) & (TW_STATUS != TW_REP_START) ) {
            continue;
        }

        TWDR = addr;
        TWCR = _BV(TWINT) | _BV(TWEN);
        while(!(TWCR & _BV(TWINT)));
        if( TW_STATUS != TW_MT_SLA_ACK ) {
            continue;
        }

        TWDR = mcp_register;
        TWCR = _BV(TWINT) | _BV(TWEN);
        while(!(TWCR & _BV(TWINT)));
        if( TW_STATUS != TW_MT_DATA_ACK) {
            continue;
        }

        TWDR = data;
        TWCR = _BV(TWINT) | _BV(TWEN);
        while(!(TWCR & _BV(TWINT)));
        if( TW_STATUS != TW_MT_DATA_ACK) {
            continue;
        }

        TWCR = _BV(TWINT) | _BV(TWEN) | _BV(TWSTO);
        while(!(TWCR & _BV(TWSTO)));
        success = 1;
        break;
    }

    if( !success ) {
        PORTD = ++errorcounter;
        return 1;
    }
    return 0;

}

void MCP23008_set(uint8_t addr, uint8_t byte) {
    MCP23008_write_error_aware(addr, GPIO_MCP23008, byte);
}


int main(void) {

    // LED init
    DDRD = 0xFF;
    PORTD = 0x00;

    // LED test
    PORTD = 0xFF;
    _delay_ms(10000);
    PORTD = 0x00;

    // TWI & MCP23008 init
    TWCR |= _BV(TWEA) | _BV(TWEN);
    MCP23008_write_error_aware(ADDR_MCP23008_1, IODIR_MCP23008, 0x00);

    MCP23008_set(ADDR_MCP23008_1, 0x00);

    while(1) {
        for(uint8_t i = 0; i <= 0xFF; i++) {
            //uint8_t r1 = random() % 0xff;
            MCP23008_set(ADDR_MCP23008_1, i);
            _delay_ms(1000);
        }
    }


    return 0;
}
